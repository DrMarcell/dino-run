#include <EEPROM.h>
#include "font6x8AJ.h"
#include <avr/sleep.h>
#include <avr/interrupt.h>

// Routines to set and clear bits (used in the sleep code)
#define	cbi(sfr, bit) (_SFR_BYTE(sfr) &= ~_BV(bit))
#define	sbi(sfr, bit) (_SFR_BYTE(sfr) |= _BV(bit))

// Defines for OLED output
#define	SSD1306XLED_H
#define	SSD1306_SCL PORTB4    // SCL, Pin 4 on webboggles.com board
#define	SSD1306_SDA PORTB3    // SDA, Pin 3
#define	SSD1306_SA  0x78    // Slave address

#define	BUTTON_LEFT  2
#define	BUTTON_RIGHT 0
#define	BUZZER       1

// Other function prototypes - for both
void doNumber (int x, int y, int value);
void ssd1306_init(void);
void ssd1306_xfer_start(void);
void ssd1306_xfer_stop(void);
void ssd1306_send_byte(uint8_t byte);
void ssd1306_send_command(uint8_t command);
void ssd1306_send_data_start(void);
void ssd1306_send_data_stop(void);
void ssd1306_setpos(uint8_t x, uint8_t y);
void ssd1306_fillscreen(uint8_t fill_Data, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2);
void ssd1306_char_f6x8(uint8_t x, uint8_t y, const char ch[]);
void ssd1306_draw_bmp(uint8_t x, uint8_t y, uint8_t *bitmap);
void ssd1306_draw_dino(uint8_t x, uint8_t y, uint8_t frame, uint8_t down, uint8_t jump);
void ssd1306_draw_cactus1(char x, char y);
void ssd1306_draw_cactus2(char x, char y);
void ssd1306_draw_bird(char x, char y, uint8_t frame);
void ssd1306_draw_dino_ground(uint8_t x, uint8_t y);
void ssd1306_draw_dirt(int x, int y, char type);
void ssd1306_draw_cloud(uint8_t x, uint8_t y);

void menu();
int game();
void scoreboard(int score);

void setup() {
  DDRB = (1 << BUZZER);

  ssd1306_init();
}

// Arduino stuff - loop
void loop() {
  menu();
  int score = game();
  scoreboard(score);
}

void menu() {
  ssd1306_fillscreen(0x00, 0, 0, 128, 8);
  ssd1306_char_f6x8(61, 2, "Dino Run");
  ssd1306_fillscreen(32, 0, 7, 128, 8);
  ssd1306_draw_dino(20, 6, 2, 0, 0);

  char wait = 1;
  while(wait) {
    ssd1306_char_f6x8(69, 4, "START");
    for(char i = 0; i < 100 && wait; i++) {
      delay(5);
      if((PINB & ((1 << BUTTON_RIGHT) | (1 << BUTTON_LEFT)))) {
        wait = 0;
        char seed = 0;
        while((PINB & ((1 << BUTTON_RIGHT) | (1 << BUTTON_LEFT)))) {
          seed++;
        }
        randomSeed(seed);
        delay(300);
      }
    }
    ssd1306_fillscreen(0x00, 69, 4, 99, 5);
    for(char i = 0; i < 100 && wait; i++) {
      delay(5);
      if((PINB & ((1 << BUTTON_RIGHT) | (1 << BUTTON_LEFT)))) {
        wait = 0;
        delay(300);
      }
    }
  }
}

int game() {
  ssd1306_fillscreen(0x00, 0, 0, 128, 8);
  ssd1306_fillscreen(32, 0, 7, 128, 8);
  delay(100);

  unsigned char frame = 0;
  char down = 0;
  char jump = 0;
  char jumpHeight = 0;
  char gamespeed = 30;
  char alive = 1;
  int xBird = 2000 + random(8000);
  int xCactus1 = 128 + random(200);
  int xCactus2 = 550 + random(200);
  char cactusType1 = 0;
  char cactusType2 = 0;
  int yDino = 6;
  int score = 0;
  char duck;
  char duckEnabled = 1;
  int xDirt1 = 130 + random(100);
  int xDirt2 = 150 + random(100);
  int xDirt3 = 190 + random(100);
  int xCloud = 200 + random(500);
  char yCloud = random(3);
  char dirtType1 = random(5);
  char dirtType2 = random(5);
  char dirtType3 = random(5);

  while(alive) {
    down = (PINB & (1 << BUTTON_LEFT)) && duckEnabled;
    if(duck > 30) {
      duckEnabled = 0;
    }
    if(down) {
      duck++;
    } else if(duck > 1) {
      duck--;
    } else {
      duckEnabled = 1;
    }
    if(jump == 0) {
      jump = (PINB & (1 << BUTTON_RIGHT));
      if(jump) {
        score++;
      }
    } else if(jump == 1) {
      if(jumpHeight < 10) {
        jumpHeight++;
        yDino = 6 - jumpHeight / 2;
      } else {
        jump = 2;
      }
    } else if(jump == 2) {
      if(jumpHeight > 0) {
        jumpHeight--;
        yDino = 6 - jumpHeight / 2;
      } else {
        jumpHeight = 0;
        jump = 0;
      }
    }
    ssd1306_draw_dino_ground(20, 7);
    ssd1306_draw_dirt(xDirt1, 7, dirtType1);
    ssd1306_draw_dirt(xDirt2, 7, dirtType2);
    ssd1306_draw_dirt(xDirt3, 7, dirtType3);
    ssd1306_draw_cloud(xCloud, yCloud);

    ssd1306_draw_dino(20, yDino, frame % 6 < 3, down, jump);
    ssd1306_draw_bird(xBird, 4, frame % 12 < 6);

    switch(cactusType1) {
      case 0: ssd1306_draw_cactus1(xCactus1, 6); break;
      case 1: ssd1306_draw_cactus2(xCactus1, 6); break;
    }
    switch(cactusType2) {
      case 0: ssd1306_draw_cactus1(xCactus2, 6); break;
      case 1: ssd1306_draw_cactus2(xCactus2, 6); break;
    }
    doNumber(1, 0, score);

    xBird -= 3;
    xCactus1 -= 4;
    xCactus2 -= 4;
    xDirt1 -= 4;
    xDirt2 -= 4;
    xDirt3 -= 4;
    xCloud--;

    if(xBird < -30) {
      xBird = 2000 + random(8000);
      score++;
    }

    if(xDirt1 < -10) {
      dirtType1 = random(5);
      xDirt1 = 130 + random(100);
    }

    if(xDirt2 < -10) {
      dirtType2 = random(5);
      xDirt2 = 130 + random(100);
    }

    if(xDirt3 < -10) {
      dirtType3 = random(5);
      xDirt3 = 130 + random(100);
    }

    if(xCloud < -30) {
      yCloud = random(3);
      xCloud = 200 + random(500);
    }

    if(xCactus1 < -30) {
      cactusType1 = random(2);
      xCactus1 = 128 + random(200);
      score++;
    }

    if(xCactus2 < -30) {
      cactusType2 = random(2);
      xCactus2 = 260 + random(200);
      score++;
    }

    if(xCactus1 > 128 && (xCactus1 - xCactus2 < 80)) {
      xCactus1 = 128 + random(200);
    }

    if(xCactus2 > 128 && (xCactus2 - xCactus1 < 80)) {
      xCactus2 = 260 + random(200);
    }

    if(xCactus1 > 128 && (xCactus1 - xBird < 240) && (xCactus1 > xBird)) {
      xCactus1 = xBird + 350 + random(240);
    }

    if(xCactus2 > 128 && (xCactus2 - xBird < 240) && (xCactus2 > xBird)) {
      xCactus2 = xBird + 350 + random(240);
    }

    if(score < 1000) {
      gamespeed = 30 - score / 50;
    }

    if((xCactus1 > 12 && xCactus1 < 38 && yDino > 4)
       || (xCactus2 > 12 && xCactus2 < 38 && yDino > 4)
       || (xBird > 12 && xBird < 30 && (yDino < 6 || down == 0))) {
      alive = 0;
    }

    if(frame % 60 == 0) {
      score++;
    }

    delay(gamespeed);
    frame++;
  }
  return score;
}

void scoreboard(int score) {
  char wait = 1;
  char flashes = 3;
  char period = 0;
  int highScore = 0;
  int scoreWasSet = EEPROM.read(0);
  if(scoreWasSet == 17) {
    highScore = word(EEPROM.read(1), EEPROM.read(2));
    scoreWasSet = 15;
  } else {
    EEPROM.write(0, 17);
    EEPROM.write(1,0);
    EEPROM.write(2,0);
  }
  if(score > highScore) {
    EEPROM.write(1,highByte(score));
    EEPROM.write(2,lowByte(score));
    highScore = score;
  }
  while(flashes--) {
    ssd1306_fillscreen(0xff, 0, 0, 128, 8);
    delay(period);
    ssd1306_fillscreen(0x00, 0, 0, 128, 8);
    delay(period);
    period += 20;
  }
  delay(750);
  ssd1306_char_f6x8(35, 0, "Game Over");
  ssd1306_char_f6x8(10, 4, "Score:");
  doNumber(52, 4, score);
  ssd1306_char_f6x8(10, 6, "High:");
  doNumber(52, 6, highScore);

  ssd1306_fillscreen(32, 0, 7, 128, 8);
  ssd1306_draw_cactus1(105, 6);

  while(wait) {
    if((PINB & ((1 << BUTTON_RIGHT) | (1 << BUTTON_LEFT)))) {
      wait = 0;
      delay(300);
    }
  }
}

void doNumber (int x, int y, int value) {
  char temp[10] = { 0,0,0,0,0,0,0,0,0,0 };
  itoa(value,temp,10);
  ssd1306_char_f6x8(x, y, temp);
}

void ssd1306_init(void) {
  DDRB |= (1 << SSD1306_SDA); // Set port as output
  DDRB |= (1 << SSD1306_SCL); // Set port as output

  ssd1306_send_command(0xAE); // display off
  ssd1306_send_command(0x00); // Set Memory Addressing Mode
  ssd1306_send_command(0x10); // 00,Horizontal Addressing Mode;01,Vertical Addressing Mode;10,Page Addressing Mode (RESET);11,Invalid
  ssd1306_send_command(0x40); // Set Page Start Address for Page Addressing Mode,0-7
  ssd1306_send_command(0x81); // Set COM Output Scan Direction
  ssd1306_send_command(0xCF); // ---set low column address
  ssd1306_send_command(0xA1); // ---set high column address
  ssd1306_send_command(0xC8); // --set start line address
  ssd1306_send_command(0xA6); // --set contrast control register
  ssd1306_send_command(0xA8);
  ssd1306_send_command(0x3F); // --set segment re-map 0 to 127
  ssd1306_send_command(0xD3); // --set normal display
  ssd1306_send_command(0x00); // --set multiplex ratio(1 to 64)
  ssd1306_send_command(0xD5); //
  ssd1306_send_command(0x80); // 0xa4,Output follows RAM content;0xa5,Output ignores RAM content
  ssd1306_send_command(0xD9); // -set display offset
  ssd1306_send_command(0xF1); // -not offset
  ssd1306_send_command(0xDA); // --set display clock divide ratio/oscillator frequency
  ssd1306_send_command(0x12); // --set divide ratio
  ssd1306_send_command(0xDB); // --set pre-charge period
  ssd1306_send_command(0x40); //
  ssd1306_send_command(0x20); // --set com pins hardware configuration
  ssd1306_send_command(0x02);
  ssd1306_send_command(0x8D); // --set vcomh
  ssd1306_send_command(0x14); // 0x20,0.77xVcc
  ssd1306_send_command(0xA4); // --set DC-DC enable
  ssd1306_send_command(0xA6); //
  ssd1306_send_command(0xAF); // --turn on oled panel
}

void ssd1306_xfer_start(void) {
  PORTB |= (1 << SSD1306_SCL);  // Set to HIGH
  PORTB |= (1 << SSD1306_SDA);  // Set to HIGH
  PORTB &= ~(1 << SSD1306_SDA);   // Set to LOW
  PORTB &= ~(1 << SSD1306_SCL);   // Set to LOW
}

void ssd1306_xfer_stop(void) {
  PORTB &= ~(1 << SSD1306_SCL);   // Set to LOW
  PORTB &= ~(1 << SSD1306_SDA);   // Set to LOW
  PORTB |= (1 << SSD1306_SCL);  // Set to HIGH
  PORTB |= (1 << SSD1306_SDA);  // Set to HIGH
}

void ssd1306_send_byte(uint8_t byte) {
  uint8_t i;
  for(i = 0; i < 8; i++)
  {
    if((byte << i) & 0x80)
      PORTB |= (1 << SSD1306_SDA);
    else
      PORTB &= ~(1 << SSD1306_SDA);

    PORTB |= (1 << SSD1306_SCL);
    PORTB &= ~(1 << SSD1306_SCL);
  }
  PORTB |= (1 << SSD1306_SDA);
  PORTB |= (1 << SSD1306_SCL);
  PORTB &= ~(1 << SSD1306_SCL);
}

void ssd1306_send_command(uint8_t command) {
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);  // Slave address, SA0=0
  ssd1306_send_byte(0x00);  // write command
  ssd1306_send_byte(command);
  ssd1306_xfer_stop();
}

void ssd1306_send_data_start(void) {
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);
  ssd1306_send_byte(0x40);  //write data
}

void ssd1306_send_data_stop(void) {
  ssd1306_xfer_stop();
}

void ssd1306_setpos(uint8_t x, uint8_t y)
{
  ssd1306_xfer_start();
  ssd1306_send_byte(SSD1306_SA);  //Slave address,SA0=0
  ssd1306_send_byte(0x00);  //write command

  ssd1306_send_byte(0xb0 + y);
  ssd1306_send_byte(((x & 0xf0) >> 4) | 0x10); // |0x10
  ssd1306_send_byte((x & 0x0f) | 0x01); // |0x01

  ssd1306_xfer_stop();
}

void ssd1306_fillscreen(uint8_t fill_Data, uint8_t x1, uint8_t y1, uint8_t x2, uint8_t y2) {
  for(uint8_t m = y1; m < y2; m++) {
    ssd1306_setpos(x1,m);
    ssd1306_send_data_start();
    for(uint8_t n = x1; n < x2; n++) {
      ssd1306_send_byte(fill_Data);
    }
    ssd1306_send_data_stop();
  }
}

void ssd1306_char_f6x8(uint8_t x, uint8_t y, const char ch[]) {
  uint8_t c,i,j = 0;
  while(ch[j] != '\0')
  {
    c = ch[j] - 32;
    if (c > 0) c = c - 12;
    if (c > 15) c = c - 6;
    if (c > 40) c = c - 9;
    if(x > 126)
    {
      x = 0;
      y++;
    }
    ssd1306_setpos(x,y);
    ssd1306_send_data_start();
    for(i = 0; i < 6; i++)
    {
      ssd1306_send_byte(pgm_read_byte(&ssd1306xled_font6x8[c * 6 + i]));
    }
    ssd1306_send_data_stop();
    x += 6;
    j++;
  }
}

void drawIfInside(uint8_t byte, int x) {
  if(x >= 0 && x < 127) {
    ssd1306_send_byte( byte );
  }
}

void ssd1306_draw_bird(int x, int y, uint8_t frame) {
  int bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y);
  ssd1306_send_data_start();
  drawIfInside( 0b01000000, bytex++);
  drawIfInside( 0b01100000, bytex++);
  drawIfInside( 0b01110000, bytex++);
  drawIfInside( 0b01111000, bytex++);
  drawIfInside( 0b01111100, bytex++);
  drawIfInside( 0b01111100, bytex++);
  drawIfInside( 0b11110000, bytex++);
  if(frame == 0) {
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b11000000, bytex++);
    drawIfInside( 0b10000000, bytex++);
  } else {
    drawIfInside( 0b11000111, bytex++);
    drawIfInside( 0b11111110, bytex++);
    drawIfInside( 0b11111100, bytex++);
    drawIfInside( 0b11111000, bytex++);
    drawIfInside( 0b11110000, bytex++);
    drawIfInside( 0b11100000, bytex++);
    drawIfInside( 0b10000000, bytex++);
    drawIfInside( 0b00000000, bytex++);
  }

  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  ssd1306_send_data_stop();

  bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y + 1);
  ssd1306_send_data_start();
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  if(frame == 0) {
    drawIfInside( 0b11111111, bytex++);
    drawIfInside( 0b01111111, bytex++);
    drawIfInside( 0b00111111, bytex++);
    drawIfInside( 0b00011111, bytex++);
  } else {
    drawIfInside( 0b00000011, bytex++);
    drawIfInside( 0b00000111, bytex++);
    drawIfInside( 0b00001111, bytex++);
    drawIfInside( 0b00001111, bytex++);
  }

  drawIfInside( 0b00001111, bytex++);
  drawIfInside( 0b00001111, bytex++);
  drawIfInside( 0b00001111, bytex++);
  drawIfInside( 0b00001111, bytex++);
  drawIfInside( 0b00001111, bytex++);
  drawIfInside( 0b00000111, bytex++);
  drawIfInside( 0b00000101, bytex++);
  drawIfInside( 0b00000101, bytex++);
  drawIfInside( 0b00000001, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  ssd1306_send_data_stop();
}

void ssd1306_draw_dirt(int x, int y, char type) {
  int bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y);
  ssd1306_send_data_start();
  switch(type) {
    case 0:
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b10100000, bytex++);
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      break;
    case 1:
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      break;
    case 2:
      drawIfInside( 0b10100000, bytex++);
      drawIfInside( 0b01100000, bytex++);
      drawIfInside( 0b10100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      break;
    case 3:
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00110000, bytex++);
      drawIfInside( 0b00110000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      break;
    case 4:
      drawIfInside( 0b11000000, bytex++);
      drawIfInside( 0b10000000, bytex++);
      drawIfInside( 0b00000000, bytex++);
      drawIfInside( 0b00000000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      drawIfInside( 0b00100000, bytex++);
      break;
  }
  ssd1306_send_data_stop();
}

void ssd1306_draw_cactus1(int x, int y) {
  int bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y);
  ssd1306_send_data_start();
  drawIfInside( 0b01111000, bytex++);
  drawIfInside( 0b11111100, bytex++);
  drawIfInside( 0b11111000, bytex++);
  drawIfInside( 0b11000000, bytex++);
  drawIfInside( 0b10000000, bytex++);
  drawIfInside( 0b11111110, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b11111110, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b11000000, bytex++);
  drawIfInside( 0b11100000, bytex++);
  drawIfInside( 0b11000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  ssd1306_send_data_stop();

  bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y + 1);
  ssd1306_send_data_start();
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100001, bytex++);
  drawIfInside( 0b00100011, bytex++);
  drawIfInside( 0b00100011, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b00101110, bytex++);
  drawIfInside( 0b00101111, bytex++);
  drawIfInside( 0b00100111, bytex++);
  drawIfInside( 0b00100011, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  ssd1306_send_data_stop();
}

void ssd1306_draw_cactus2(int x, int y) {
  int bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y);
  ssd1306_send_data_start();
  drawIfInside( 0b11100000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b10000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b11110000, bytex++);
  drawIfInside( 0b10000000, bytex++);
  drawIfInside( 0b11111110, bytex++);
  drawIfInside( 0b11111100, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  ssd1306_send_data_stop();

  bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y + 1);
  ssd1306_send_data_start();
  drawIfInside( 0b00100111, bytex++);
  drawIfInside( 0b00101100, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b00110000, bytex++);
  drawIfInside( 0b00111100, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100001, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b11111111, bytex++);
  drawIfInside( 0b00110000, bytex++);
  drawIfInside( 0b00111100, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  drawIfInside( 0b00100000, bytex++);
  ssd1306_send_data_stop();
}

void ssd1306_draw_dino_ground(uint8_t x, uint8_t y) {
  ssd1306_setpos(x, y);
  ssd1306_send_data_start();
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_byte( 0b00100000 );
  ssd1306_send_data_stop();
}

void ssd1306_draw_cloud(int x, uint8_t y) {
  int bytex = x;
  ssd1306_setpos(x > 0 ? x : 0, y);
  ssd1306_send_data_start();
  drawIfInside( 0b01100000, bytex++);
  drawIfInside( 0b10010000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10010000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10000100, bytex++);
  drawIfInside( 0b10000010, bytex++);
  drawIfInside( 0b10000010, bytex++);
  drawIfInside( 0b10000010, bytex++);
  drawIfInside( 0b10001100, bytex++);
  drawIfInside( 0b10010000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b10001000, bytex++);
  drawIfInside( 0b01110000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  drawIfInside( 0b00000000, bytex++);
  ssd1306_send_data_stop();
}

void ssd1306_draw_dino(uint8_t x, uint8_t y, uint8_t frame, uint8_t down, uint8_t jump) {
  if(jump == 2) {
    ssd1306_setpos(x, y - 1);
    ssd1306_send_data_start();
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_data_stop();
  }
  ssd1306_setpos(x, y);
  ssd1306_send_data_start();
  if(down == 0 || jump) {
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11100000 );
    ssd1306_send_byte( 0b11111110 );
    ssd1306_send_byte( 0b11111111 );
    ssd1306_send_byte( 0b11110011 );
    ssd1306_send_byte( 0b01111111 );
    ssd1306_send_byte( 0b01011111 );
    ssd1306_send_byte( 0b01011111 );
    ssd1306_send_byte( 0b01011111 );
    ssd1306_send_byte( 0b00011110 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
  } else {
    ssd1306_send_byte( 0b01000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b10000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11000000 );
    ssd1306_send_byte( 0b11100000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b00110000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b11110000 );
    ssd1306_send_byte( 0b11100000 );
  }
  ssd1306_send_data_stop();

  ssd1306_setpos(x, y + 1);
  ssd1306_send_data_start();
  if(jump == 0) {
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100011 );
    ssd1306_send_byte( 0b00000111 );
    ssd1306_send_byte( 0b00001111 );
  } else {
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000011 );
    ssd1306_send_byte( 0b00000111 );
    ssd1306_send_byte( 0b00001111 );
  }

  if(jump == 0) {
    switch(frame) {
      case 0:
        ssd1306_send_byte( 0b00011111 );
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b00011111 );
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b11111111 );
        ssd1306_send_byte( 0b10011111 );
        break;
      case 1:
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b11111111 );
        ssd1306_send_byte( 0b10011111 );
        ssd1306_send_byte( 0b00011111 );
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b00011111 );
        break;
      case 2:
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b11111111 );
        ssd1306_send_byte( 0b10011111 );
        ssd1306_send_byte( 0b00111111 );
        ssd1306_send_byte( 0b11111111 );
        ssd1306_send_byte( 0b10011111 );
        break;
    }
  } else {
    ssd1306_send_byte( 0b00111111 );
    ssd1306_send_byte( 0b11111111 );
    ssd1306_send_byte( 0b10011111 );
    ssd1306_send_byte( 0b00111111 );
    ssd1306_send_byte( 0b11111111 );
    ssd1306_send_byte( 0b10011111 );
  }

  ssd1306_send_byte( 0b00001111 );
  if(jump) {
    ssd1306_send_byte( 0b00000010 );
    ssd1306_send_byte( 0b00000110 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
  } else if(down == 0) {
    ssd1306_send_byte( 0b00000010 );
    ssd1306_send_byte( 0b00100110 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
    ssd1306_send_byte( 0b00100000 );
  } else {
    ssd1306_send_byte( 0b00011111 );
    ssd1306_send_byte( 0b00011111 );
    ssd1306_send_byte( 0b00000111 );
    ssd1306_send_byte( 0b00000111 );
    ssd1306_send_byte( 0b00100111 );
    ssd1306_send_byte( 0b00100101 );
    ssd1306_send_byte( 0b00100101 );
    ssd1306_send_byte( 0b00100101 );
    ssd1306_send_byte( 0b00100001 );
  }
  ssd1306_send_data_stop();

  if(jump == 1) {
    ssd1306_setpos(x, y + 2);
    ssd1306_send_data_start();
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_byte( 0b00000000 );
    ssd1306_send_data_stop();
  }
}
